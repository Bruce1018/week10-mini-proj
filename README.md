# week10-mini-proj

> zx112  
> Zilin Xu

# requirements and corresponding demo

## Dockerize Hugging Face Rust transformer

0. create a new `cargo project`

1. change the `.toml` file, add the dependencies we needed

```toml
[dependencies]
lambda_http = "0.11.1"
serde = {version = "1.0", features = ["derive"] }
serde_json = "1.0"
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
rand = "0.8.5"
openssl = { version = "0.10.35", features = ["vendored"] }

tokio = { version = "1", features = ["macros"] }


```

2. pick up a model from **Hugging Face**, I picked `rustformers/redpajama-3b-ggml`, download the `.bin`

3. modify the `main.rs` to get response from the model as following

![](/截屏2024-04-19%2015.09.41.png)

4. create `Dockerfile` and run the commands 

![](/截屏2024-04-19%2015.14.23.png)

## Deploy container to AWS Lambda

0. Deploy the cargo project on Lambda as before

1. Create API

![](/截屏2024-04-19%2015.19.17.png)

## Implement query endpoint

using the above work and url to test the project

![](/截屏2024-04-19%2015.20.12.png)